/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f10x.h"
#include <math.h>
#include <stdlib.h>

char buff[10];
uint8_t buff_flag,wsk;
int x,y,dl;
float kat;

volatile uint32_t timer;
uint32_t i;

void SysTick_Handler()
{
	if(timer) timer--;
}

void delay_ms(uint32_t time)
{
	timer = time;
	while(timer);
}

void Lmotor(int pwm);
void Rmotor(int pwm);


int main(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	GPIO_InitTypeDef gpio;
	GPIO_StructInit(&gpio);
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_Init(GPIOC,&gpio);

	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13;
	GPIO_Init(GPIOB,&gpio);

	gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	gpio.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_Init(GPIOA, &gpio);

	gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	gpio.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_Init(GPIOB, &gpio);

	gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	gpio.GPIO_Pin = GPIO_Pin_2;
	GPIO_Init(GPIOA, &gpio);

	gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	gpio.GPIO_Pin = GPIO_Pin_3;
	GPIO_Init(GPIOA, &gpio);

	TIM_TimeBaseInitTypeDef tim;
	TIM_TimeBaseStructInit(&tim);
	tim.TIM_CounterMode = TIM_CounterMode_Up;
	tim.TIM_Prescaler = 36 - 1;
	tim.TIM_Period = 100;
	TIM_TimeBaseInit(TIM3, &tim);

	TIM_OCInitTypeDef oc;
	TIM_OCStructInit(&oc);
	oc.TIM_OCMode = TIM_OCMode_PWM1;
	oc.TIM_OutputState = TIM_OutputState_Enable;
	oc.TIM_Pulse = 0;
	TIM_OC1Init(TIM3, &oc);
	TIM_OC2Init(TIM3, &oc);
	TIM_OC3Init(TIM3, &oc);
	TIM_OC4Init(TIM3, &oc);

	USART_InitTypeDef usart;
	USART_StructInit(&usart);
	usart.USART_BaudRate = 9600;
	USART_Init(USART2, &usart);

	SysTick_Config(SystemCoreClock/1000);

	USART_Cmd(USART2, ENABLE);
	TIM_Cmd(TIM3, ENABLE);

	GPIO_SetBits(GPIOC, GPIO_Pin_14);
	GPIO_ResetBits(GPIOC, GPIO_Pin_13);
	GPIO_SetBits(GPIOC, GPIO_Pin_15);

	GPIO_SetBits(GPIOC, GPIO_Pin_12);
	GPIO_SetBits(GPIOC, GPIO_Pin_13);

	while(1)
	{

		if(USART_GetFlagStatus(USART2, USART_FLAG_RXNE))
		{
			char c = USART_ReceiveData(USART2);

			/*if (c == '1') GPIO_SetBits(GPIOC, GPIO_Pin_14);
			else if (c == '2') GPIO_ResetBits(GPIOC, GPIO_Pin_14);*/

			if(c==';')
			{
				buff_flag=1;
				wsk=0;
			}
			else buff[wsk++]=c;
		}

		if(buff_flag)
		{
			if(buff[0]=='x')
			{
				x=atoi(&buff[1]);
				while(buff[wsk++]!='y');
				y=atoi(&buff[wsk]);

				if(x >= 100)
				{
					if(y >= 100)
					{
						Rmotor((y-100) - (x - 100));
						Lmotor((y-100) + (x - 100));
					}
					else
					{
						Rmotor((-100 + y) - (x - 100));
						Lmotor((-100 + y) + (x - 100));
					}

				}
				else
				{
					if(y >= 100)
					{
						Rmotor((y-100) + (100 - x));
						Lmotor((y-100) - (100 - x));
					}
					else
					{
						Rmotor((-100 + y) + (100 - x));
						Lmotor((-100 + y) - (100 - x));
					}
				}

			 	GPIO_WriteBit(GPIOC, GPIO_Pin_13, 1 - GPIO_ReadOutputDataBit(GPIOC, GPIO_Pin_13));
			 	wsk=0;
			 	for(uint8_t i=0;i<10;i++) buff[i]=0;
			 	buff_flag=0;
			}
		}
	}
}

void Lmotor(int pwm)
{
	if(pwm > 100) pwm = 100;
	if(pwm < -100) pwm = -100;

	if(pwm >= 0)
	{
		TIM_SetCompare1(TIM3, pwm);
		TIM_SetCompare2(TIM3, 0);
	}
	else
	{
		TIM_SetCompare1(TIM3, 0);
		TIM_SetCompare2(TIM3, abs(pwm));
	}
}

void Rmotor(int pwm)
{
	if(pwm > 100) pwm = 100;
	if(pwm < -100) pwm = -100;

	if(pwm >= 0)
	{
		TIM_SetCompare3(TIM3, pwm);
		TIM_SetCompare4(TIM3, 0);
	}
	else
	{
		TIM_SetCompare3(TIM3, 0);
		TIM_SetCompare4(TIM3, abs(pwm));
	}
}
